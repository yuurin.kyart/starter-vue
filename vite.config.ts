import { resolve } from 'node:path'
import { unheadVueComposablesImports } from '@unhead/vue'
import vue from '@vitejs/plugin-vue'
import unocss from 'unocss/vite'
import autoImport from 'unplugin-auto-import/vite'
import vueComponents from 'unplugin-vue-components/vite'
import { VueRouterAutoImports } from 'unplugin-vue-router'
import vueRouter from 'unplugin-vue-router/vite'
import { defineConfig } from 'vite'
import vueLayouts from 'vite-plugin-vue-layouts'

// https://vitejs.dev/config/
export default defineConfig(({ command }) => ({
  plugins: [
    // https://unocss.dev/
    unocss(),
    // https://github.com/antfu/unplugin-auto-import
    autoImport({
      imports: [
        'vue',
        'vue-router',
        '@vueuse/core',
        VueRouterAutoImports,
        unheadVueComposablesImports,
      ],
      dts: 'src/auto-imports.d.ts',
    }),
    // https://github.com/JohnCampionJr/vite-plugin-vue-layouts
    vueLayouts(),
    // // https://github.com/posva/unplugin-vue-router
    vueRouter({
      exclude: command === 'build' ? ['src/pages/examples/**'] : [],
      dts: 'src/auto-routes.d.ts',
    }),
    // https://github.com/vitejs/vite/tree/main/packages/plugin-vue
    vue(),
    // https://github.com/unplugin/unplugin-vue-components
    vueComponents({
      dts: 'src/auto-components.d.ts',
    }),
  ],
  resolve: {
    alias: {
      '~/': `${resolve(__dirname, './src')}/`,
      '@/': `${resolve(__dirname, './src')}/`,
    },
  },
  ssgOptions: {
    formatting: 'minify',
    dirStyle: 'nested',
  },
}))
